import React, { useEffect, useState } from 'react';
import URLSearchParams from '@ungap/url-search-params' // polyfill

export const QueryStringRouterContext = React.createContext();

export function QueryStringRouterProvider({ children }) {
  const [params, setParams] = useState(paramsAsObject(new URLSearchParams(window.location.search)));

  useEffect(() => {
    const handler = (event) => {
      setParams(event.state);
    };

    window.addEventListener("popstate", handler);

    if (window.history.state === null) {
      let params = paramsAsObject(new URLSearchParams(window.location.search));
      window.history.replaceState(params, "");
    }

    return () => {
      window.removeEventListener("popstate", handler);
    };
  }, []);

  const props = {
    value: {
      params,
      setAllParams: (newParams) => setAllParams(setParams, newParams)
    }
  };
  return React.createElement(QueryStringRouterContext.Provider, props, children);
}

function paramsAsObject(params) {
  var obj = {};
  for (const [key, value] of params) {
    obj[key] = value;
  }
  return obj;
}

function setAllParams(setParams, newParams) {
  const url = new URL(window.location.href);
  url.search = "?" + new URLSearchParams(newParams).toString();

  window.history.pushState(newParams, "", url.toString());
  setParams(newParams);
}
