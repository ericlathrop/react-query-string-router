import React from 'react';
import { Counter } from "./Counter";
import { QueryStringRouterProvider } from "@ericlathrop/react-query-string-router";
import './App.css';

function App() {
  return (
    <QueryStringRouterProvider>
      <Counter />
    </QueryStringRouterProvider>
  );
}

export default App;
