import React, { useContext } from 'react';
import { QueryStringRouterContext } from "@ericlathrop/react-query-string-router";

export function Counter() {
  const { params, setAllParams } = useContext(QueryStringRouterContext);
  const counter = parseInt(params.counter) || 0;

  return (
    <div>
      <div>
        Counter is {counter}
      </div>
      <div>
        <button onClick={() => setAllParams({ counter: counter + 1 })}>
          Increment counter
        </button>
      </div>
    </div>
  );
}
